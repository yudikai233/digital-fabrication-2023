# Digital Fabrication 2023

Repository for the Digital Fabrication courses

- AXM-E7009
- AXM-E7010
- AXM-E7011 

## DF II Students

... to be completed

### New Students

1. [repo](https://gitlab.com/nagisamizuno/digital-fabrication-2023) [www](https://nagisamizuno.gitlab.io/digital-fabrication-2023/) Nagisa Mizuno
1. [repo](https://gitlab.com/yudikai233/test001/) [www](https://yudikai233.gitlab.io/test001/) Dikai Yu

## DF I Students

1. [repo](https://gitlab.com/noo-ra/digital-fabrication/) [www](https://noo-ra.gitlab.io/digital-fabrication/) Noora Bosch Salonen
1. [repo](https://gitlab.com/DamlaSerper/digital-fabrication) [www](https://damlaserper.gitlab.io/digital-fabrication/) Damla Serper
1. [repo](https://gitlab.com/hiskihuovila/digital-fabrication/) [www](https://hiskihuovila.gitlab.io/digital-fabrication) Hiski Huovila
1. [repo](https://gitlab.com/rwaltaalto/DigitalFabrication/) [www](https://rwaltaalto.gitlab.io/DigitalFabrication/) Richard Van der Walt
1. [repo](https://gitlab.com/luupanu/luupanu.gitlab.io) [www](https://luupanu.gitlab.io) Panu Luukkonen
1. [repo](https://gitlab.com/Elpiq/digital-fabrication) [www](https://elpiq.gitlab.io/digital-fabrication/) Elise Piquemal
1. [repo](https://gitlab.com/pegahshamlou/digifab/) [www](https://pegahshamlou.gitlab.io/digifab/) Pegah Shamloo
1. [repo](https://gitlab.com/thvo.fi/website-demo/) [www](https://thvo.fi.gitlab.io/website-demo/) Thanh Vo
1. [repo](https://gitlab.com/arianamarta/digital-fabrication/) [www](https://arianamarta.gitlab.io/digital-fabrication/) Ariana Marta
1. [repo](https://gitlab.fabcloud.org/academany/fabacademy/2023/labs/aalto/students/saskia-helinska) [www](https://fabacademy.org/2023/labs/aalto/students/saskia-helinska/) Saskia Helinska
1. [repo](https://gitlab.com/burakturkoz/digital-fabrication-by-burak/) [www](https://burakturkoz.gitlab.io/digital-fabrication-by-burak/) Burak Türköz
1. [repo](https://gitlab.com/sarisko/digital-fabrication/) [www](https://sarisko.gitlab.io/digital-fabrication/) Sara Kutkova
1. [repo](https://gitlab.com/YunhaoZhong/digital-fabrication) [www](https://yunhaozhong.gitlab.io/digital-fabrication/) Yunhao Zhong (Lucas)
1. [repo](https://gitlab.com/poonamchawda/digital-fabrication) [www](https://poonamchawda.gitlab.io/digital-fabrication/) Poonam Chawda
1. [repo](https://gitlab.com/YunhaoZhong/digital-fabrication) [www](https://aw1996428.gitlab.io/digital-fabrication-anqi/) Anqi Wang
1. [repo](https://gitlab.com/Zhicheng_Wang/zhicheng_wang.gitlab.io) [www](https://zhicheng_wang.gitlab.io) Zhicheng Wang
1. [repo](https://gitlab.com/gabrielafarias/digital-fabrication-website/) [www](https://gabrielafarias.gitlab.io/digital-fabrication-website/) Gabriela Farias
1. [repo](https://gitlab.com/miyajik1/digital-fabricaiton_kazuma-miyajima/) [www](https://miyajik1.gitlab.io/digital-fabricaiton_kazuma-miyajima/) Kazuma Miyajima
